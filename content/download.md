---
layout: download
title: Download
appstream: org.kde.glaxnimate
flatpak: org.kde.glaxnimate
#gear: glaxnimate
name: Glaxnimate
menu:
  main:
    weight: 10
description: Glaxnimate is available as a precompiled package in a wide range of platforms. You can check the package status for your Linux distro on the right or keep reading for info on other operating systems
scssFiles:
- /scss/download.scss
sources:
- name: Appimage
  src_icon: /reusable-assets/appimage.svg
  description: |
    Download the latest Appimage version of Glaxnimate from [our servers](https://gitlab.com/api/v4/projects/19921167/jobs/artifacts/release/raw/build/glaxnimate-x86_64.AppImage?job=linux%3Aappimage).

    Make sure the AppImage is executable `chmod a+x glaxnimate-x86_64.AppImage`. Running the AppImage should start Glaxnimate.

    If your system doesn't support mounting user level filesystems use the following commands instead:
    ```bash
    ./glaxnimate-x86_64.AppImage --appimage-extract
    ./squashfs-root/AppRun
    ```
- name: Snap
  src_icon: /reusable-assets/snapcraft.svg
  description: |
    You can install the latest version of Glaxnimate from [Snapcraft Store](https://snapcraft.io/glaxnimate).
- name: Deb package
  src_icon: /reusable-assets/ubuntu.svg
  description: |
    Download the latest Deb package of Glaxnimate from [our servers](https://gitlab.com/api/v4/projects/19921167/jobs/artifacts/release/raw/build/glaxnimate.deb?job=linux%3Adeb). Open the deb with your package manager and follow the instructions to install it.

    Note that the deb package has a dependency to python3.10.
- name: AUR
  src_icon: /images/icons/arch.svg
  description: |
    For the stable package:
    ```bash
    git clone https://aur.archlinux.org/glaxnimate.git
    cd glaxnimate
    makepkg -rsi
    ```
    (For unstable replace `glaxnimate` by `glaxnimate-git`)
- name: FreeBSD
  src_icon: /images/icons/freebsd.png
  description: |
    Simply install Glaxnimate via `pkg`:
    ```bash
    pkg install glaxnimate
    ```
- name: Microsoft Store (Windows)
  src_icon: /reusable-assets/windows.svg
  description: "The [Microsoft Store](https://www.microsoft.com/store/apps/9n41msq1wnm8) is the recommended place to install Glaxnimate, the version there has been tested by our developers and the Microsoft Store provides seamless updates when new versions are released."
- name: Windows installer
  src_icon: /reusable-assets/windows.svg
  description: |
    Download the latest Deb package of Glaxnimate from [our servers](https://github.com/mbasaglia/glaxnimate/releases/download/master/glaxnimate-x86_64.zip).
- name: macOS
  src_icon: /reusable-assets/macos.svg
  description: |
    Download the latest version of Glaxnimate for macOS from [our servers](https://github.com/mbasaglia/glaxnimate/releases/download/release/glaxnimate.dmg).

    Open (mount) the dmg file, then either open glaxnimate.app to run it or drag it to Applications to install it.

    You may get a warning "Apple cannot check app for malicious software", please see [install KDE apps on macOS](https://community.kde.org/Mac) for details.
- name: PyPi
  src_icon: /images/icons/pypi.svg
  description: |
    This package is for the python module only, not for the full program. You can get it on [PyPi](/contributing/scripting).
- name: Release Sources
  src_icon: /reusable-assets/ark.svg
  description: |
    If you want to build from source, you can check the [README](https://invent.kde.org/graphics/glaxnimate/-/blob/master/README.md).
- name: Development sources
  src_icon: /reusable-assets/git.svg
  description: |
    The source code is available on [KDE's GitLab](https://invent.kde.org/graphics/glaxnimate), this is this is the place where development happens. Check the [README](https://invent.kde.org/graphics/glaxnimate/-/blob/master/README.md) for details.
- name: Development Snapshots
  src_icon: /images/icons/applications-development.svg
  description: |
    These packages are built continuously as new changes are made. **They contain all the latest features but might also include bugs and broken features.**
    - Binaries for Windows and macOS as well as Appimage are available on https://cdn.kde.org/ci-builds/graphics/glaxnimate.
    - To download a unstable Flatpak use the [nightly repository](https://cdn.kde.org/flatpak/glaxnimate-nightly/org.kde.glaxnimate.flatpakref).
    - Experimental Android APK's are available from the CI artifacts in KDE's GitLab

---
