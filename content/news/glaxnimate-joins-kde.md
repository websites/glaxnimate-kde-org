---
date: 2024-05-01
title: Glaxnimate joins KDE!
draft: true
---
The Glaxnimate team is proud to announce Glaxnimate is now part of KDE. Glaxnimate benefits from the shared KDE build and distribution infrastructure, the collective knowledge of the community and libraries such as KDE Frameworks. This way the developers can spend more time on the code to fix bugs and develop new features for you! Stay tuned!

