---
title: 'Glaxnimate roadmap: an Emoji guide'
hideMeta: true
author: Mattia Basaglia
menu:
  main:
    name: Roadmap
    parent: contributing
    weight: 80
---

Just a silly little list of things I currently plan and their status. It isn't meant to be exhaustive.

For more details, you can check the [GitLab Board](https://invent.kde.org/graphics/glaxnimate/-/boards/13683?label_name[]=Feature%20Request).

### Legend
* ✔️ = Done
* 🔧 = Work in progress
* 🐍 = Not done for Glaxnimate but available in python-lottie
* ❌ = To do

## Features

* ✔️ Extensible Open / Save Infrastructure
* ✔️ Custom Format Open / Save
* ✔️ Layer Management
* ✔️ Python Scripting Extensibility
* ✔️ Plugin system
* ✔️ Lottie Open / Save
* ✔️ SVG Open / Save
* ✔️ Synfig Open / Save
* ✔️ Gif/WebP Export
* ✔️ Copy/Paste within Glaxnimate
* ✔️ Copy/Paste SVG
* ✔️ Copy/Paste Images
* ✔️ Basic Shapes
* ✔️ Vector Editing
* ✔️ Basic Animations
* 🐍 Preset Animation Effects
* ✔️ Document-wide palette
* ✔️ Precompositions
