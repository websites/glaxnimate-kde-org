---
title: Links
hideMeta: true
author: Mattia Basaglia
layout: link-lists
menu:
  main:
    name: More Links
    parent: links
    weight: 100
lists:
  - title: Listings
    items:
    - label: AlternativeTo
      link: https://alternativeto.net/software/glaxnimate/
    - label: FSF Directory
      link: https://directory.fsf.org/wiki/Glaxnimate
    - label: fresh(code)
      link: https://freshcode.club/projects/glaxnimate
    - label: Open Hub
      link: https://www.openhub.net/p/glaxnimate
    - label: SourceForge
      link: https://sourceforge.net/projects/glaxnimate/
  - title: Reviews and Articles
    items:
    - label: 0.4.3 Video comparing various animation programs (Russian)
      link: https://www.youtube.com/watch?v=26V3T9QqYWw
    - label: 0.3.1 Dev.to
      link: https://dev.to/mbasaglia/glaxnimate-create-2d-vector-animations-for-the-web-2ein
    - label: 0.3.0 Softpedia
      link: https://www.softpedia.com/get/Multimedia/Graphic/Graphic-Others/Glaxnimate.shtml
    - label: 0.3.0 Apps for my PC
      link: http://www.appsformypc.com/2020/11/glaxnimate/
    - label: 0.2.0 Emojized
      link: https://emojized.com/blog/2020/10/29/new-awesome-tool-for-animated-telegram-stickers/
---
