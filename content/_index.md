---
title: Glaxnimate - Fast and simple vector graphics editor
layout: home

features:
  - title: Customizable Interface
    icon: /preferences-desktop-theme-global.svg
    description: |
      Dark and Light UI themes, Icon themes and dockable views.
    link: https://docs.glaxnimate.org/en/user_interface/settings.html

  - title: Smooth Animations
    icon: draw-bezier-curves.svg
    description: |
      Vector graphics and tweening animations
    link: https://docs.glaxnimate.org/en/formats.html

  - title: Cross Platform
    icon: computer.svg
    description: |
      Download for GNU/Linux, Windows, and Mac
    link: 'download'

  - title: Animations for the web
    icon: internet-web-browser.svg
    description: |
      Lottie animations, Animated GIF and WebP and Animated SVG.
    link: https://docs.glaxnimate.org/en/formats.html

  - title: Easily Extensible
    icon: preferences-plugin.svg
    description: |
      You can use Python to manipulate animations and create plugins.
    link: /contributing/scripting/

learnmore:
  - title: User Manual
    link: https://docs.glaxnimate.org
  - title: Scripting Guide
    link: /contributing/scripting
---


