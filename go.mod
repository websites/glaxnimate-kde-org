module glaxnimate-org

go 1.19

require (
	github.com/airbnb/lottie-web v5.12.2+incompatible // indirect
	invent.kde.org/websites/hugo-kde v0.0.0-20250119182848-ee83bd1fc3e1 // indirect
)
